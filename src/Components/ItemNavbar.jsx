import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav,Form,FormControl,Button,Navbar, Container} from 'react-bootstrap';
import './css/style.css'
import { Link } from 'react-router-dom';
export default function ItemNavbar() {
    return (
       
        <Navbar bg="dark" expand="lg">
             <Container>
                <Navbar.Brand as={Link} to="/">React-Router</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as ={Link} to="/home">Home</Nav.Link>
                        <Nav.Link as={Link} to="/videos">Video</Nav.Link>
                        <Nav.Link as={Link} to="/account">Account</Nav.Link>
                        <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
       
    )
}
