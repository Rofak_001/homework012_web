import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button } from "react-bootstrap";
import { Link} from "react-router-dom";
export default function ItemCard(props) {
  return (
      
    <Card className="mt-4">
      <Card.Img variant="top" src={props.image} />
      <Card.Body>
        <Card.Title>Naruto</Card.Title>
        <Card.Text>
          This is a wider card with supporting text below as a natural lead-in
          to additional content. This content is a little bit longer.
        </Card.Text>
        <Button variant="outline-success">
          <Link to={ `/posts/`+props.id} style={{ textDecoration: "none" }}>
            See More
          </Link>
        </Button>
      </Card.Body>
      <Card.Footer>
        <small className="text-muted">Last updated 3 mins ago</small>
      </Card.Footer>
    </Card>
  );
}
