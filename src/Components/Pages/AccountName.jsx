import React from 'react'
export default function AccountName({name}) {
  
    return (
        <div>
             {name? (<h3>The <span style={{color:"blue"}}>name</span> in query string is "{name}"</h3>):(<h3>There is no name in the query string</h3>)}   
        </div>
    )
}
