import React from 'react'
import ItemCard from '../ItemCard'
import { Row, Col,Container } from 'react-bootstrap'
import Image1 from '../Image/Naruto.jpg'
import Image2 from '../Image/Naruto1.jpg'
import Image3 from '../Image/Naruto2.jpg'
import Image4 from '../Image/Naruto3.jpg'
import Image5 from '../Image/Naruto4.jpg'
import Image6 from '../Image/Naruto5.jpg'
import Image7 from '../Image/Naruto6.png'
import Image8 from '../Image/Naruto7.png'
export default function Home() {
    
    return (
        <div>
            <Container>
                <Row>
                    <Col md={3}>
                        <ItemCard image={Image1} id="1"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image2} id="2"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image3} id="3"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image4} id="4"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image5} id="5"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image6}id="6"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image7} id="7"/>
                    </Col>
                    <Col md={3}>
                        <ItemCard image={Image8} id="8"/>
                    </Col>
                </Row>
            </Container> 
        </div>
    )
}
