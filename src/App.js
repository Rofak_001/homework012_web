
import ItemNavbar from './Components/ItemNavbar';
import { Switch,BrowserRouter, Route } from 'react-router-dom';
import Main from './Components/Pages/Main';
import Home from './Components/Pages/Home';
import Detail from './Components/Pages/Detail';
import NotFond from './Components/Pages/NotFond';
import Vides from './Components/Pages/Vides';
import Account from './Components/Pages/Account';
import AnimateCategory from './Components/Pages/AnimateCategory';
import MovieCategory from './Components/Pages/MovieCategory';
import Welcome from './Components/Pages/Welcome';
import Auth from './Components/Pages/Auth';
import React, { Component } from 'react'
import { ProtectedRoute } from './Components/Pages/Route/Proected';

export default class App extends Component {
  render() {
    return (
      <div>
      <BrowserRouter>
          <ItemNavbar/>
          <Switch>
              <Route exact path="/"  component={Main}/>
              <Route path="/home"  component={Home}/>
              <Route path="/videos" component={Vides}/>
              <Route path="/account"  component={Account}/>           
              <Route path="/auth"  render={()=><Auth />}/>
              <Route path="/posts/:id"  component={Detail}/>
              <ProtectedRoute exact path="/welcome" component={Welcome}/>
              <Route path="/videos/animation"  component={AnimateCategory}/>
              <Route path="/videos/movie"  component={MovieCategory}/>
              <Route path="*" component={NotFond} />
          </Switch>
      </BrowserRouter>       
    </div>
    )
  }
}


